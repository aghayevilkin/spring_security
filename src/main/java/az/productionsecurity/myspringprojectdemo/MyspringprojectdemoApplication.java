package az.productionsecurity.myspringprojectdemo;

import az.productionsecurity.myspringprojectdemo.domain.User;
import az.productionsecurity.myspringprojectdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class MyspringprojectdemoApplication implements CommandLineRunner {


	private final UserRepository userRepository;
	private final BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args)  {
		SpringApplication.run(MyspringprojectdemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		log.info("Creating demo user");
//		User user = new User();
//		user.setUsername("ilkin");
//		user.setPassword(passwordEncoder.encode("123"));
//		userRepository.save(user);
	}
}
