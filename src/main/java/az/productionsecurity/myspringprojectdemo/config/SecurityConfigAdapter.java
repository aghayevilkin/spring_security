package az.productionsecurity.myspringprojectdemo.config;

import az.productionsecurity.myspringprojectdemo.services.MyUserDetailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private final MyUserDetailService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/register**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/hello**")
                .authenticated()
                .antMatchers("/user**")
                .hasAnyRole("USER", "ADMIN")
                .antMatchers("/admin**")
                .hasAnyRole("ADMIN");
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("obama").password(bCryptPasswordEncoder().encode("123")).roles("ADMIN");
        auth.inMemoryAuthentication().withUser("kamil").password(bCryptPasswordEncoder().encode("123")).roles("USER");
//        log.info("Injecting user details service{}", userDetailsService);
        auth.userDetailsService(userDetailsService);
    }


}
