package az.productionsecurity.myspringprojectdemo.config;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SessionConfig {
    @Bean
    public ServletListenerRegistrationBean<SessionListenerWithMetrics> sessionListenerWithMetrics() {
        ServletListenerRegistrationBean<SessionListenerWithMetrics> listenerRegBean =
                new ServletListenerRegistrationBean<>();

        listenerRegBean.setListener(new SessionListenerWithMetrics());
        return listenerRegBean;
    }
}
