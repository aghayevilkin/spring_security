package az.productionsecurity.myspringprojectdemo.config;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class  SessionListenerWithMetrics implements HttpSessionListener {
    private final AtomicInteger activeSessions;

    public SessionListenerWithMetrics() {
        super();

        activeSessions = new AtomicInteger();
    }

    public int getTotalActiveSession() {
        return activeSessions.get();
    }

    public void sessionCreated(final HttpSessionEvent event) {
        log.info("Session created {}",event.getSession().getId());
        activeSessions.incrementAndGet();
        log.info("Number of session {}",activeSessions);
    }
    public void sessionDestroyed(final HttpSessionEvent event) {
        log.info("Session destroyed");
        activeSessions.decrementAndGet();
        log.info("Number of session {}",activeSessions);
    }
}
