package az.productionsecurity.myspringprojectdemo.collection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.rmi.RemoteException;


@Slf4j
@Component
@Order(1)
public class CustomFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.trace("Custom INGRESS filter created {}", filterConfig);

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.trace("INGRESS FILTER request source {}", request.getRemoteAddr());
//        log.trace("INGRESS FILTER request source {}",request);
//        if (request instanceof HttpServletRequest) {
//            String url = ((HttpServletRequest)request).getRequestURL().toString();
//            String queryString = ((HttpServletRequest)request).getQueryString();
//        }
        chain.doFilter(request, response);
    }
    @Override
    public void destroy() {
        log.trace("Custom INGRESS filter destroyed ");
    }
}
