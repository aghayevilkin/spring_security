package az.productionsecurity.myspringprojectdemo.collection;

import az.productionsecurity.myspringprojectdemo.domain.User;
import az.productionsecurity.myspringprojectdemo.services.HelloService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HelloController {

    private final HelloService helloService;

    @GetMapping("/public")  //world access, no authentication or authorization
    public String sayHellopublic() {
//        log.trace("public api invoked bla bla bla");
//        log.info("public api invoked");
//        try {
//            int check = (1 / 0);
//        } catch (Exception e) {
//            log.error("Operation Faild!", e);
//            return "Faild to great!";
//        }
        return "Hello World Public";
    }

    @PostMapping("/register")
    public boolean register(User user){
        return helloService.register(user);
    }

    @GetMapping("/hello") //only  authentication no authorization
    public String sayHello() {
        return helloService.sayHello();
    }

    @GetMapping("/user")  // authentication and  only users are authorization to access
    public String apiForUser() {
        return "Hello User";
    }

    @GetMapping("/admin") //  authentication and  only admins are authorization to access
    public String apiForAdmin() {

        log.info("ind admin endpoint");
        return "Hello Admin";
    }

}
