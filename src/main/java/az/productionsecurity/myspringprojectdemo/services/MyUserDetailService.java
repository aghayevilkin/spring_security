package az.productionsecurity.myspringprojectdemo.services;

import az.productionsecurity.myspringprojectdemo.domain.User;
import az.productionsecurity.myspringprojectdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MyUserDetailService implements UserDetailsService {


    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading user {}", username);
        return userRepository.findByUsername(username)
                .map(user -> new MyUserPrincipal(user)).orElseThrow(() -> new UsernameNotFoundException(username));

    }
}
