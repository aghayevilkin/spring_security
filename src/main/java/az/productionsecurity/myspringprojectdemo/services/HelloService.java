package az.productionsecurity.myspringprojectdemo.services;

import az.productionsecurity.myspringprojectdemo.domain.User;
import az.productionsecurity.myspringprojectdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloService {

    private final UserRepository repo;
    private final BCryptPasswordEncoder passwordEncoder;

    @Secured("ADMIN")
    public String sayHello() {
        return "Hello World 1";
    }

    public boolean register(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        repo.save(user);
        return true;
    }
}
